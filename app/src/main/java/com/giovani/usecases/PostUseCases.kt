package com.giovani.usecases

import androidx.lifecycle.MutableLiveData
import com.giovani.models.Post
import com.giovani.repositories.PostRepository

class PostUseCases {

    private val repository = PostRepository()

    fun getPostsList(posts: MutableLiveData<List<Post>>) {
        repository.getPosts(true, posts)
    }
}