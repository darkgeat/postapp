package com.giovani.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.giovani.models.Post
import com.giovani.postapp.R
import kotlinx.android.synthetic.main.fragment_detail.*
import kotlinx.android.synthetic.main.toolbar.*

class DetailFragment : Fragment() {

    private var post: Post? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar?.title = "Detail Post"
        post = arguments?.get("post") as Post?
        if (post != null) {
            textViewTitlePost.text = post?.title
            textViewId.text = "id: ${post?.id}"
            textViewDescriptionPost.text = post?.body
        }
    }

}