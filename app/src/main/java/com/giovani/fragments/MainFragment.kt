package com.giovani.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.giovani.postapp.R
import com.giovani.postapp.databinding.FragmentMainBinding
import com.giovani.viewmodels.PostViewModel
import kotlinx.android.synthetic.main.toolbar.*

class MainFragment : Fragment() {

    private val viewModel : PostViewModel by lazy {
        ViewModelProviders.of(this).get(PostViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding : FragmentMainBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_main, container, false)
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        binding.executePendingBindings()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.title = "Post List"
    }
}