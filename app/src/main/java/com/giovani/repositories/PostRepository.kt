package com.giovani.repositories

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.giovani.interfaces.PostsAPIService
import com.giovani.models.Post
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PostRepository {

    private var posts = MutableLiveData<List<Post>>()

    fun getPosts(forceToDownload: Boolean, postsMutable: MutableLiveData<List<Post>>) {
        posts = postsMutable
        if (forceToDownload) {
            getPostsFromAPI()
        } else {
            // here is the logic if you want to use database
        }
    }

    private fun getPostsFromAPI() {
        val postAPI by lazy { PostsAPIService.create() }
        CoroutineScope(Dispatchers.IO).launch {
            val request = postAPI.getPosts()
            val response = request.await()
            if (response.isSuccessful) {
                var postsList : List<Post>
                response.body()?.let {
                    postsList = it
                    posts.postValue(postsList)
                }
                Log.d("getPostsFromAPI","Was successful")
            } else {
                Log.d("getPostsFromAPI","Terrible news + ${response.raw()}")
            }
        }
    }
}