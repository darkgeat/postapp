package com.giovani.databinding

import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.giovani.adapters.PostAdapter
import com.giovani.models.Post

@BindingAdapter("android:postItems")
fun setMovieAdapter(recycler: RecyclerView, items: LiveData<List<Post>>) {
    recycler.adapter?.let { adapter ->
        if (adapter is PostAdapter) {
            items.observe(recycler.context as AppCompatActivity, Observer {
                adapter.refreshDataPosts(it)
            })
        }

    }
}

@BindingAdapter("android:adapter")
fun setAdapter(recycler: RecyclerView, adapter: PostAdapter?) {
    adapter?.let {
        recycler.adapter = it
    }
}

@BindingAdapter("android:setLinearLayout")
fun setLayout(recycler: RecyclerView, isLinearLayout: Boolean) {
    recycler.setHasFixedSize(true)
    if (isLinearLayout) {
        recycler.layoutManager = LinearLayoutManager(recycler.context as AppCompatActivity)
    } else {
        recycler.layoutManager = GridLayoutManager(recycler.context as AppCompatActivity, 2)
    }
}