package com.giovani.interfaces

import com.giovani.models.Post
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

const val BASE_URL = "https://jsonplaceholder.typicode.com"

interface PostsAPIService {

    @GET("/posts")
    fun getPosts(): Deferred<Response<List<Post>>>


    companion object {
        fun create() : PostsAPIService {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .baseUrl(BASE_URL)
                .build()
            return retrofit.create(PostsAPIService::class.java)
        }
    }
}