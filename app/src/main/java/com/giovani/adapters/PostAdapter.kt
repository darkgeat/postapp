package com.giovani.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.giovani.models.Post
import com.giovani.postapp.R
import kotlinx.android.synthetic.main.item_post.view.*

class PostAdapter : RecyclerView.Adapter<PostAdapter.PostHolder>() {

    private var postItems = ArrayList<Post>()
    private var context : Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostHolder {
        context = parent.context
        val inflater =  LayoutInflater.from(context)
        return PostHolder(inflater.inflate(R.layout.item_post, parent, false))
    }

    override fun onBindViewHolder(holder: PostHolder, position: Int) {
        holder.titlePost.text = postItems[position].title
        holder.bodyPost.text = postItems[position].body
        holder.parent.setOnClickListener{
            val extras = bundleOf( "post" to postItems[position])
            it.findNavController().navigate(R.id.action_mainFragment_to_detailFragment, extras)
        }
    }

    override fun getItemCount(): Int {
        return postItems.size
    }

    fun refreshDataPosts(list: List<Post>) {
        postItems = ArrayList(list)
        notifyDataSetChanged()
    }

    inner class PostHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val parent = itemView
        val titlePost = itemView.textTitleItem
        val bodyPost = itemView.textDescriptionItem
    }
}