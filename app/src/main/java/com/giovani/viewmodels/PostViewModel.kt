package com.giovani.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.giovani.adapters.PostAdapter
import com.giovani.models.Post
import com.giovani.usecases.PostUseCases
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class PostViewModel : ViewModel() {

    private lateinit var posts: MutableLiveData<List<Post>>
    private val postsUseCases = PostUseCases()
    var adapter = PostAdapter()

    private val job = Job()
    private val scope = CoroutineScope(job + Dispatchers.Main)

    init {
        refresh()
    }

    private fun refresh() {
        scope.launch {
            getPosts()
        }
    }

    fun getPosts() : LiveData<List<Post>> {
        if (!::posts.isInitialized) {
            posts = MutableLiveData()
            postsUseCases.getPostsList(posts)
        }
        return posts
    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
}